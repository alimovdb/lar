import Vue from 'vue';

import {
  LayoutPlugin,
  LinkPlugin,
  ButtonPlugin,
  FormPlugin,
  FormGroupPlugin,
  FormInputPlugin,
  FormCheckboxPlugin,
  FormTextareaPlugin,
  SpinnerPlugin,
  ToastPlugin,
  ModalPlugin
} from 'bootstrap-vue';

Vue.use(LayoutPlugin);
Vue.use(LinkPlugin);
Vue.use(ButtonPlugin);
Vue.use(FormPlugin);
Vue.use(FormGroupPlugin);
Vue.use(FormInputPlugin);
Vue.use(FormCheckboxPlugin);
Vue.use(FormTextareaPlugin);
Vue.use(SpinnerPlugin);
Vue.use(ToastPlugin);
Vue.use(ModalPlugin);
