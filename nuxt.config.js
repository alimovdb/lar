export default {
  mode: 'universal',

  head: {
    title: 'Система управления задачами - пример проекта',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'пример'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  loading: { color: '#0174b4' },

  css: [{ src: '~assets/sass/app.scss', lang: 'scss' }],

  plugins: [{ src: '~/plugins/yt-embed.js', ssr: false }],

  axios: {
    baseURL: process.env.API_URL || 'http://mysite.com/api'
  },
  env: {
    baseSiteUrl: process.env.SITE_URL || 'http://mysite.com'
  },
  buildModules: ['@nuxtjs/eslint-module'],

  modules: ['bootstrap-vue/nuxt', '@nuxtjs/svg-sprite', '@nuxtjs/axios'],

  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    componentPlugins: [
      'LayoutPlugin',
      'LinkPlugin',
      'ButtonPlugin',
      'FormPlugin',
      'FormGroupPlugin',
      'FormInputPlugin',
      'FormCheckboxPlugin',
      'FormTextareaPlugin',
      'SpinnerPlugin',
      'ToastPlugin',
      'ModalPlugin'
    ]
  },

  svgSprite: {
    input: '~/assets/images/icons'
  },

  router: {
    base: process.env.DEPLOY_ENV === 'STATIC' ? '/' : '/'
  },

  build: {
    transpile: ['vee-validate/dist/rules'],
    extend(config, ctx) {}
  }
}
